import { createWebHistory, createRouter } from "vue-router"
import Home from "../views/Home.vue"
import Episodes from "../views/Episodes.vue"


const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/eps/:episodio",
    name: "Episodes",
    component: Episodes,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;

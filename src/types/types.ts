export type intervalo = {
  int?: number
  url: string
  name: string
  video: number
  descripton: string
}
export interface episodio extends intervalo {
  ep: number
  movimento?: string
}
export type ep = episodio | intervalo
export type State = {eps: ep[]}
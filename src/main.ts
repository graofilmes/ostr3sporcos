import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store/store"
import vueVimeoPlayer from 'vue-vimeo-player'
import VueGtag from "vue-gtag-next"

const app = createApp(App)
app.use(vueVimeoPlayer)
app.use(VueGtag, {
  property: {
    id: "G-B8T3XZF63F"
  }
})
app.use(router)
app.use(store)
app.mount('#app')

import { createStore } from "vuex"
import { State } from '../types/types'

const state: State = {
  eps: [
    {
      ep: 0,
      url: 'prologo',
      name: 'Prólogo',
      video: 442011715,
      descripton: 'Os Tr3s Porcos saem da sede da Próxima Companhia, no bairro de Campos Elíseos, no centro de São Paulo e iniciam sua jornada por diversas cidades do estado a fim de encontrar ressonâncias do espetáculo com a pluralidade e potência das tantas lutas por moradia que vão aparecer pelo caminho.'
    },
    {
      ep: 1,
      url: 'sao-paulo',
      name: 'São Paulo',
      movimento: 'MSTC – Movimento Sem-Teto do Centro',
      video: 442011801,
      descripton: 'Na cidade de São Paulo, Os Tr3s Porcos visitam a Ocupação 9 de julho. O momento é de festa/mobilização pela liberdade de Preta e Sidney, arbitrariamente presos por lutar pelo direito coletivo à moradia digna. A luta não para nesta ocupação que é um marco e exemplo de organização do MSTC na cidade.'
    },
    {
      ep: 2,
      url: 'sao-bernardo',
      name: 'São Bernardo do Campo',
      movimento: 'MTST – Movimento dos Trabalhadores Sem Teto',
      video: 442011882,
      descripton: 'Na cidade de São Bernardo do Campo, Os Tr3s Porcos encontram um terreno recém conquistado pela luta do MTST para a construção de moradias populares. Com muita chuva, e a presença de lideranças do movimento, a troca é intensa e a conversa elucida alguns pontos chave da luta do movimento.'
    },
    {
      ep: 3,
      url: 'guarulhos',
      name: 'Guarulhos',
      movimento: 'MTST – Movimento dos Trabalhadores Sem Teto',
      video: 442012003,
      descripton: 'Na ocupação Hugo Cháves, na cidade de Guarulhos, Os Tr3s Porcos conhecem uma ocupação ainda com as casas de madeira, aguardando a resolução do Estado para a construção definitiva de uma vila. Sol forte, boas conversas e muitas crianças acompanham a visita.'
    },
    {
      int: 1,
      url: 'cheiro',
      name: 'Que Cheiro é Esse?',
      video: 442017907,
      descripton: 'Trocando muitas experiências com parcerias que encontraram pelo caminho, Os Tr3s Porcos refletem sobre o processo vivo e contínuo de construção do espetáculo de teatro de rua. E a pergunta continua no ar: onde está o lobo que tanto devora?'
    },
    {
      ep: 4,
      url: 'sorocaba',
      name: 'Sorocaba',
      video: 442012087,
      descripton: 'Chegando na Vila União, na cidade de Sorocaba, Os Tr3s Porcos encontram esta ocupação que enfrenta dificuldades para ter acesso a direitos básicos como saúde, educação e principalmente, a moradia. Mas seguem em luta constante com muita mobilização coletiva, fazendo justiça ao nome da ocupação.'
    },
    {
      ep: 5,
      url: 'poa',
      name: 'Poa ou Ferraz de Vasconcelos',
      video: 442012191,
      descripton: 'Os Tr3s Porcos se encontram perdidos na divisa entre Poá e Ferraz de Vasconcelos. Na Vila São Francisco, conhecida como Raspadão, um território que nenhum dos dois municípios quer assumir, a população segue em luta pela regularização dos serviços básicos para as habitações.'
    },
    {
      int: 2,
      url: 'cidades-perfeitas',
      name: 'As Cidades Perfeitas',
      video: 451207752,
      descripton: 'Como cada cidade constrói suas narrativas? Os Tr3s Porcos, em sua trajetória pelo estado de São Paulo, encontram cidades com realidades que não necessariamente condizem com o discurso criado sobre elas. Em um dia de sol que vira temporal, eles se perguntam: a quem interessa esse discurso?'
    },
    {
      ep: 6,
      url: 'sao-jose',
      name: 'São José dos Campos',
      video: 451210249,
      descripton: 'Em um território ocupado há muito tempo junto à natureza, na cidade de São José dos Campos. Os Tr3s Porcos encontram moradores antigos, que estão em luta constante com os interesses do mercado imobiliário, sempre se debatendo com a questão: o Banhado fica ou não fica?'
    },
    {
      ep: 7,
      url: 'piracicaba',
      name: 'Piracicaba',
      video: 451211975,
      descripton: 'Por essa não esperavam. Os Tr3s Porcos chegam à comunidade dos Três Porquinhos! Na cidade de Piracicaba, mesmo com chuva, o espetáculo não pode parar. As crianças dividem guarda-chuvas e salgadinhos, e um encontro potente reforça a importância da cultura na luta popular.'
    },
    {
      int: 3,
      url: 'luta',
      name: 'LutA',
      video: 456616384,
      descripton: 'Em sua jornada pelas cidades, Os Tr3s Porcos não puderam deixar de perceber e celebrar a força de tantas mulheres que seguem à frente dos movimentos de luta por moradia. Resistência é a tônica da vida e trajetória de quem assume a frente de uma luta que é coletiva e interseccional.'
    },
    {
      ep: 8,
      url: 'bauru',
      name: 'Bauru',
      movimento: 'MSLT – Movimento Social de Luta dos Trabalhadores',
      video: 451214877,
      descripton: 'Pelo caminho, às vezes se apresentam alguns percalços. A ocupação Nova Canaã se encontra em processo de reintegração de posse e os conflitos de um movimento que precisa agir frente à uma remoção obriga Os Tr3s Porcos a repensar trechos do espetáculo para que o diálogo com a luta se mantenha vivo.'
    },
    {
      ep: 9,
      url: 'carapicuiba',
      name: 'Carapicuiba',
      movimento: 'MTST – Movimento dos Trabalhadores Sem Teto',
      video: 451559904,
      descripton: 'Na ocupação Carlos Marighella, em Carapicuíba, uma caminhada traz à tona mais alguns pontos importantes da luta do MTST. Os Tr3s Porcos encontram realidades muito próximas às apresentadas no espetáculo. Construções e reconstruções são uma realidade presente na vida de quem acredita e sonha.'
    },
    {
      ep: 10,
      url: 'ribeirao-preto',
      name: 'Ribeirão Preto',
      movimento: 'UMM – União dos Movimentos de Moradia',
      video: 451585534,
      descripton: 'Na cidade de Ribeirão Preto, Os Tr3s Porcos chegam à Cidade Locomotiva. Casas dentro de vagões de trem caracterizam essa ocupação histórica da UMM. Ameaças aumentam com uma mudança de governo, e novas estratégias precisam ser criadas para que as sementes de luta possam continuar crescendo.'
    },
    {
      int: 4,
      url: 'lobotomia',
      name: 'Lobotomia',
      video: 456627331,
      descripton: 'Ocupações, vias públicas, espaços institucionais… O que Os Tr3s Porcos costumam encontrar ao apresentar um espetáculo em espaços tão diversos. Encontros conflituosos e inusitados aparecem pelo caminho, e lembre-se: Cuidado com a rede do Lobo, antes que ele te veja, fuja, senão… Lobotomia!'
    },
    {
      ep: 11,
      url: 'presidente-prudente',
      name: 'Presidente Prudente',
      movimento: 'MST – Movimento dos Trabalhadores Sem-Terras<br />FPTAI – Federação Prudentina de Teatro e Artes Integradas',
      video: 451587618,
      descripton: 'Uma conversa sobre o MST na visita dos Tr3s Porcos à cidade de Presidente Prudente traz reflexões sobre as lutas do campo e da cidade. No Galpão da Lua uma conversa importante sobre a luta e potência dessa ocupação cultural. É noite, mas mesmo quando o sol está escondido, ele ainda está lá.'
    },
    {
      ep: 12,
      url: 'santo-andre',
      name: 'Santo André',
      video: 451588181,
      descripton: 'Os Tr3s Porcos encontram no Jardim Alzira Franco em Santo André, uma história de conquistas por meio de muita luta para urbanizar o bairro. Um respiro que mostra que o diálogo constante com a comunidade e a cobrança do poder público pode viabilizar algumas possibilidades de uma vida melhor.'
    },
    {
      int: 5,
      url: 'teto',
      name: 'O Teto Prometido',
      video: 451622194,
      descripton: 'Os Tr3s Porcos conhecem a Casa do MTST, em uma conversa fundamental para entender alguns pontos básicos do movimento, o olhar se volta para a fé e as motivações para se manter na luta. E a comida deliciosa das cozinhas coletivas é um exemplo de que a força coletiva pode mobilizar muita coisa!'
    },
    {
      ep: 13,
      url: 'caieiras',
      name: 'Caieiras',
      video: 451591381,
      descripton: 'Depois de apresentar o espetáculo, Os Tr3s Porcos vão à Igreja! Não para rezar para Nossa Senhora dos Rabicós Torcidos, mas para conversar na Associação Santa Clara sobre a importância do envolvimento popular nas conquistas. Uma visita ao canteiro de obras mostra os percalços dos 18 anos de luta.'
    },
    {
      ep: 14,
      url: 'sumare',
      name: 'Sumaré',
      movimento: 'MTST – Movimento dos Trabalhadores Sem Teto',
      video: 451594081,
      descripton: 'Enquanto morar for um privilégio, ocupar é um dever. Na Vila Soma, após uma assembleia decisiva e comemorativa do MTST, Os Tr3s Porcos tem uma conversa sobre a função social dos terrenos que ajuda a entender um pouco o processo de conquista da moradia justa. Quem não pode com a Formiga…'
    },
    {
      ep: 15,
      url: 'taboao',
      name: 'Taboão',
      movimento: 'MTST – Movimento dos Trabalhadores Sem Teto',
      video: 451596081,
      descripton: 'A alegria de conquistar a moradia está presente nesse episódio onde Os Tr3s Porcos conhecem o Residencial João Cândido, conquistado com muita luta e que hoje abriga tantas famílias. A descrença e desesperança se apresentam constantemente na luta, por isso é importantíssimo celebrar as vitórias.'
    },
    {
      url: 'epilogo',
      name: 'Epílogo',
      video: 456775158,
      descripton: 'De volta à metrópole de onde saíram, Os Tr3s Porcos refletem sobre a experiência de troca com tantos lugares visitados. Eles e o espetáculo saem transformados e a pergunta continua: Com tanta casa vazia, por que tem tanta gente sem casa? A batalha é constante e a luta continua!'
    },
  ]
}

const store = createStore({
   state
})

export default store